Fork from https://github.com/delordson/AuditManager.git by Delordson Kallon

- The Audit Manager App is targeted at businesses who need to maintain standards for regulatory and compliance purposes.
- upgraded with VS2015

## Audit Manager

The Audit Manager App is targeted at businesses who need to maintain standards for regulatory and compliance purposes. 
That’s all of us right? The Audit Manage App allows organisation to schedule a series of audits of their facilities, 
procedures and processes, to assign auditors, to capture audit findings and to track corrective and preventative actions 
arising from those audit findings. 

![AuditHome](Resources/AuditHome.png)

![AuditHome[1].png](Resources/AuditHome.png)

## Modules

The Audit Manager App Modules includes a Home Page, the Manage Audits Module,  the Manage Auditors Module, 
the All Audit Findings Module, the My Audit Findings Module, the My Finding Actions Module and the Settings Module.  

## Access

Access is controlled through a log in screen. The administrator uses an associated Silverlight application to configure new users, 
create roles and permissions for those roles as well as to assign users to roles.

## The Home Screen

Access to all the features of the Audit Manager App is from the home page. From here, users can navigate to the 
‘Manage Audits Module’, the ‘Manage Auditors Module’, the ‘All Audit Findings Module’, the ‘My Audit Findings Module’, 
the ‘My Finding Actions Module’ and the ‘Settings Module’.

![screenshot_11052013_070514](Resources/screenshot_11052013_070514.png)


## Manage Audits

Clicking or tabbing on the Manage Audits button takes the user to the ‘Browse Audits’ Page. On this page, the user of presented 
with a list of all audits past and future. A search box is present for quickly navigating to a required audit. 

![screenshot_11052013_064819](Resources/screenshot_11052013_064819.png)

Clicking or tabbing on any audit listed takes the user to a page for managing that audit. This page provides for full details of 
the audit, including a scheduled date, a date started and a date completed. The page is laid out in a series of tabs. 
The first tab presents key information about the audit such as its reference code, a description, state and end dates and a status.

![screenshot_11052013_065241](Resources/screenshot_11052013_065241.png)

Of course the built in LightSwitch validation of required fields and field lengths works great as expected…

![screenshot_11052013_065303](Resources/screenshot_11052013_065303.png)

…but we’ve also added multi field validation where it makes sense. So you can’t complete an audit before it has started.

![screenshot_11052013_065251](Resources/screenshot_11052013_065251.png)

The Auditors tab is where auditors are assigned. Any of your employees can be assigned as the auditors for a given audit.

![screenshot_11052013_065312](Resources/screenshot_11052013_065312.png)

![screenshot_11052013_065318](Resources/screenshot_11052013_065318.png)

The Audit Findings Tab is where any audit non compliances and audit failings are recorded.

![screenshot_11052013_065323](Resources/screenshot_11052013_065323.png)

Clicking or tabbing on an audit finding takes the user to the audit findings management page.

![screenshot_11052013_065330](Resources/screenshot_11052013_065330.png)

The page is laid out in a series of tabs. The details tab provides key information about the audit finding, including a reference 
number, a description of the finding, a person the finding is assigned to for investigation and resolution as well as target dates. 
As soon as an audit finding is assigned to a member of staff they get an automatic email informing them they have been so assigned. 

![AuditFindingAssignedEmail](Resources/AuditFindingAssignedEmail.png)

The Impact tab allows the auditors to record the impact this audit finding potentially has on the business.

[![screenshot_11052013_065335](Resources/screenshot_11052013_065335.png)

The Immediate Action tab provides a place for the person the Finding is assigned to indicate those first urgent initial and 
immediate steps they are going to take to resolve the issue. This is not necessarily the full resolution but a quick way of 
reducing risk. The full corrective and preventative actions are added to the CAPA tab. 

![screenshot_11052013_065338](Resources/screenshot_11052013_065338.png)

The Further Actions tab is where additional steps which need to be take to resolve the issue are listed. These Further Actions 
can be assigned to other Employees. 

![screenshot_11052013_065347](Resources/screenshot_11052013_065347.png)

![screenshot_11052013_065342](Resources/screenshot_11052013_065342.png)

Again Employees get an email as soon as a Further Action is assigned to them.
The CAPA tab is where the full Corrective Action and Preventative Action (CAPA) is recorded. CAPA’s have a target date by which 
they need to be completed.

![screenshot_11052013_065514](Resources/screenshot_11052013_065514.png)

Finally there is a Notes tab for capturing any additional information such as lessons learn for example.

![screenshot_11052013_065518](Resources/screenshot_11052013_065518.png)

## Manage Auditors

The Manage Auditors module is where Auditor history is captured. The module is accessed by clicking or tabbing on the 
‘Manage Auditors’ button from the home page. This brings the user to the ‘Browse Auditors’ page. 

![screenshot_11052013_065536](Resources/screenshot_11052013_065536.png)

Clicking or tabbing on any of these auditors takes the user to the auditor management page. This page is laid out in two tabs. 
A details tab where key details of the auditor such as name and date trained… 

![screenshot_11052013_065540](Resources/screenshot_11052013_065540.png)

…and an audits undertaken tab, which lists all audits that the auditor has been involved in. 

![screenshot_11052013_065543](Resources/screenshot_11052013_065543.png)

Clicking or tabbing on a listed audit takes the user to the same audit management page described earlier…

![screenshot_11052013_065241](Resources/screenshot_11052013_065241.png)


## All Audit Finding

The All Audit Findings module provides a list of all finding across all audits and is a good place for reviewing finding which 
have not yet been closed. The All Findings Module is accessed from the home page by clicking or tabbing on the ‘All Audit Findings’ 
button. This takes the user to the ‘All Audit Findings’ page. A search facility is present to aid navigation to the required finding. 

![screenshot_11052013_065553](Resources/screenshot_11052013_065553.png)

Clicking or tabbing on any of these findings takes the user to the same Audit Findings management page described earlier.

![screenshot_11052013_065330](Resources/screenshot_11052013_065330.png)


## My Audit Findings

The My Audit Findings module provides a list of all finding across all audits that have been assigned specifically to the logged 
in user. The My Audit Findings Module is accessed from the home page by clicking or tabbing on the ‘My Audit Findings’ button. 
This takes the user to the ‘Browse My Assigned Audit Findings’ page. Again clicking or tabbing on any of these findings takes 
the user to the same Audit Findings management page described earlier.

![screenshot_11052013_065603](Resources/screenshot_11052013_065603.png)


## My Finding Actions

The My Finding Actions module provides a list of all further actions, across all audit findings, across all audits, that have 
been assigned specifically to the logged in user. The My Finding Actions Module is accessed from the home page by clicking or 
tabbing on the ‘My Finding Actions’ button. This takes the user to the ‘Browse My Audit Finding Actions’ page. 

![screenshot_11052013_065609](Resources/screenshot_11052013_065609.png)

Clicking or tabbing on any of these finding actions opens a the same Further Action Edit screen shown earlier.

## Settings

The Settings Module is the key module used by the administrator and any other users given the right permissions for managing key application reference data. The Settings module is accessed by clicking or tabbing on the Settings button on the home page. This takes the user to the settings page. 

![screenshot_11052013_065614](Resources/screenshot_11052013_065614.png)

The main items managed are the Audit Types, the Audit Statuses and the Employees.     

**Audit Types**

![screenshot_11052013_065618](Resources/screenshot_11052013_065618.png)


**Audit Statuses**

![screenshot_11052013_065624](Resources/screenshot_11052013_065624.png)


**Employees**

![screenshot_11052013_065631](Resources/screenshot_11052013_065631.png)